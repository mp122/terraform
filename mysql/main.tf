
resource "azurerm_mysql_server" "main" {
  name                = "mysql-server-maulik"
  location            = var.location
  resource_group_name = var.resourcegroup

  sku {
    name     = "B_Gen5_2"
    capacity = 2
    tier     = "Basic"
    family   = "Gen5"
  }

  storage_profile {
    storage_mb            = 5120
    backup_retention_days = 7
    geo_redundant_backup  = "Disabled"
  }

  administrator_login          = "testadmin"
  administrator_login_password = "Password1234!"
  version                      = "5.7"
  ssl_enforcement              = "Enabled"
}

resource "azurerm_mysql_database" "test" {
  name                = "exampledb"
  resource_group_name = var.resourcegroup
  server_name         = "${azurerm_mysql_server.main.name}"
  charset             = "utf8"
  collation           = "utf8_unicode_ci"
}

resource "azurerm_mysql_firewall_rule" "mysqlfirewall" {
  name                = "testrule"
  resource_group_name = var.resourcegroup
  server_name         = azurerm_mysql_server.main.name
  start_ip_address    = var.ip_address
  end_ip_address      = var.ip_address
}


resource "local_file" "Inventory_file_create" {
  content  = "Target ansible_host=${var.ip_address} ansible_ssh_pass=Password1234! ansible_user=testadmin ansible_ssh_common_args='-o StrictHostKeyChecking=no'"
  filename = "../test/inv.txt"
}
