variable "prefix" {
  default = "tfvmex_1"
}

resource "azurerm_resource_group" "main" {
  name     = "${var.prefix}-resources"
  location = "West US"
}

module "virtual_machine" {
  source = "./virtual_machine"

  location = azurerm_resource_group.main.location

  resourcegroup = azurerm_resource_group.main.name
  prefix        = var.prefix
}

module "mysql" {
  source        = "./mysql"
  public_ip_add = "${module.virtual_machine.pubip}"
  location      = azurerm_resource_group.main.location

  resourcegroup = azurerm_resource_group.main.name
  ip_address    = module.virtual_machine.pubip
}
resource "null_resource" "execute_ansible_file" {
  depends_on = ["module.mysql"]
  provisioner "local-exec" {
    command = "ansible-playbook test_ansible.yaml -i inv.txt"
  }
}
